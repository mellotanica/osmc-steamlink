#!/bin/bash

LAUNCHER_PATH=$( cd $(dirname "${BASH_SOURCE[0]}") && pwd )

if [ ! -f /usr/bin/steamlink ]; then
    sudo su -c "bash ${LAUNCHER_PATH}/install.sh"
fi

echo ${LAUNCHER_PATH}
sudo su osmc -c "bash ${LAUNCHER_PATH}/heartbeat.sh &" &
#sudo vcgencmd hdmi_channel_map 0x0BFac4c8
sudo su osmc -c "openvt -c 7 -s -f /usr/bin/steamlink & sudo /opt/vc/bin/vcgencmd hdmi_channel_map 0x0BFac4c8" &
sudo openvt -c 7 -s -f clear

sleep 2

sudo su -c "systemctl stop mediacenter &" &

exit
