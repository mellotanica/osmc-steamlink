#!/bin/bash

echo 'deb http://archive.raspberrypi.org/debian/ stretch main ui' > /etc/apt/sources.list.d/steramlink.list
wget http://archive.raspberrypi.org/debian/raspberrypi.gpg.key -O - | apt-key add -
apt-get -y update

apt-get -y install steamlink

sed -i 's/sudo apt-get install/export PATH="\/sbin:$PATH"\nsudo apt-get -y install/' usr/bin/steamlinkdeps
sed -i '/read line/d' usr/bin/steamlinkdeps
